'use strict';

(function () {
    "use strict";

    angular.module('app', ['app.constants', 'app.run', 'app.config', 'app.routes', 'app.controllers', 'app.directives', 'app.filters', 'app.factories', 'app.services', 'app.api', 'ui.bootstrap', 'ngSanitize']);

    angular.module('app.constants', []);
    angular.module('app.run', []);
    angular.module('app.config', []);
    angular.module('app.routes', ['ui.router']);
    angular.module('app.controllers', []);
    angular.module('app.directives', []);
    angular.module('app.filters', []);
    angular.module('app.factories', []);
    angular.module('app.services', []);
    angular.module('app.api', []);
})();
// (function () {
//     "use strict";


//     angular.module('app.config')


//         .config(/*@ngInject*/function ($qProvider) {
//             $qProvider.errorOnUnhandledRejections(true);
//         })


//         .config(/*@ngInject*/
//         function ($httpProvider) {
//             $httpProvider.interceptors.push('Interceptor');
//         });


// })();
"use strict";
'use strict';

(function () {
    'use strict';

    angular.module('app.constants').constant("SITE_CONSTANTS", window.SITE_CONSTANTS);
})();
'use strict';

(function () {
    AppCtrl.$inject = ["$rootScope", "$window"];
    angular.module('app.controllers').controller('AppCtrl', AppCtrl);

    /* @ngInject */
    function AppCtrl($rootScope, $window) {

        if ($window.innerWidth < 991) {
            $rootScope.hlkey_use_paceholder = true;
        } else {
            $rootScope.hlkey_use_paceholder = false;
        }

        // $rootScope.getView = window.getView;
        // $rootScope.getCommonView = window.getCommonView;
    }
})();
'use strict';

(function () {
    "use strict";

    angular.module('app.routes').config(["$stateProvider", "$urlRouterProvider", "$locationProvider", function ($stateProvider, $urlRouterProvider, $locationProvider) {
        $locationProvider.html5Mode(true);

        $urlRouterProvider.otherwise('/error');

        $stateProvider.state('home', {
            url: '/',
            templateUrl: getView('home'),
            controller: 'HomeCtrl',
            controllerAs: 'HC'
        }).state('error', {
            url: '/error',
            templateUrl: getView('home'),
            controller: 'HomeCtrl',
            controllerAs: 'HC'
        });
    }]);
})();
'use strict';

function isUndefined(data) {
    return typeof data === 'undefined';
}

function getView(moduleName, viewName) {
    if (isUndefined(viewName)) {
        viewName = moduleName;
    }
    return './views/modules/' + moduleName + '/' + viewName + '.html?v=' + window.html_version;
}

function getNestedView(moduleName, viewName) {
    if (isUndefined(viewName)) {
        viewName = moduleName;
    }

    return './views/modules/' + moduleName + '/' + viewName + '/' + viewName + '.html?v=' + window.html_version;
}

function getCommonView(moduleName, viewName) {
    if (isUndefined(viewName)) {
        viewName = moduleName;
    }

    return './views/common/' + moduleName + '/' + viewName + '.html?v=' + window.html_version;
}
'use strict';

(function () {
    'use strict';

    HomeCtrl.$inject = ["$state", "$window", "SITE_CONSTANTS"];
    angular.module('app.controllers').controller('HomeCtrl', HomeCtrl);

    /* @ngInject */
    function HomeCtrl($state, $window, SITE_CONSTANTS) {
        var vm = this;
        activate();

        function activate() {}
    }
})();
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImluZGV4LmpzIiwiYXBwLmNvbmZpZy5qcyIsImFwcC5jb25zdGFudHMuanMiLCJhcHAuY29udHJvbGxlci5qcyIsInJvdXRlcy5qcyIsInV0aWxzL3V0aWxzLmpzIiwibW9kdWxlcy9ob21lL2hvbWUuY29udHJvbGxlci5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7QUFFQSxDQUFBLFlBQUE7SUFDQTs7SUFFQSxRQUFBLE9BQUEsT0FBQSxDQUFBLGlCQUFBLFdBQUEsY0FBQSxjQUFBLG1CQUFBLGtCQUFBLGVBQUEsaUJBQUEsZ0JBQUEsV0FBQSxnQkFBQTs7SUFFQSxRQUFBLE9BQUEsaUJBQUE7SUFDQSxRQUFBLE9BQUEsV0FBQTtJQUNBLFFBQUEsT0FBQSxjQUFBO0lBQ0EsUUFBQSxPQUFBLGNBQUEsQ0FBQTtJQUNBLFFBQUEsT0FBQSxtQkFBQTtJQUNBLFFBQUEsT0FBQSxrQkFBQTtJQUNBLFFBQUEsT0FBQSxlQUFBO0lBQ0EsUUFBQSxPQUFBLGlCQUFBO0lBQ0EsUUFBQSxPQUFBLGdCQUFBO0lBQ0EsUUFBQSxPQUFBLFdBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0dBO0FDbkJBOztBQUVBLENBQUEsWUFBQTtJQUNBOztJQUVBLFFBQUEsT0FBQSxpQkFBQSxTQUFBLGtCQUFBLE9BQUE7O0FDTEE7OztpREFFQSxDQUFBLFlBQUE7SUFDQSxRQUFBLE9BQUEsbUJBQUEsV0FBQSxXQUFBOzs7SUFHQSxTQUFBLFFBQUEsWUFBQSxTQUFBOztRQUVBLElBQUEsUUFBQSxhQUFBLEtBQUE7WUFDQSxXQUFBLHVCQUFBO2VBQ0E7WUFDQSxXQUFBLHVCQUFBOzs7Ozs7O0FDWEE7O0FBRUEsQ0FBQSxZQUFBO0lBQ0E7O0lBRUEsUUFBQSxPQUFBLGNBQUEscUVBQUEsVUFBQSxnQkFBQSxvQkFBQSxtQkFBQTtRQUNBLGtCQUFBLFVBQUE7O1FBRUEsbUJBQUEsVUFBQTs7UUFFQSxlQUFBLE1BQUEsUUFBQTtZQUNBLEtBQUE7WUFDQSxhQUFBLFFBQUE7WUFDQSxZQUFBO1lBQ0EsY0FBQTtXQUNBLE1BQUEsU0FBQTtZQUNBLEtBQUE7WUFDQSxhQUFBLFFBQUE7WUFDQSxZQUFBO1lBQ0EsY0FBQTs7OztBQ25CQTs7QUFFQSxTQUFBLFlBQUEsTUFBQTtJQUNBLE9BQUEsT0FBQSxTQUFBOzs7QUFHQSxTQUFBLFFBQUEsWUFBQSxVQUFBO0lBQ0EsSUFBQSxZQUFBLFdBQUE7UUFDQSxXQUFBOztJQUVBLE9BQUEscUJBQUEsYUFBQSxNQUFBLFdBQUEsYUFBQSxPQUFBOzs7QUFHQSxTQUFBLGNBQUEsWUFBQSxVQUFBO0lBQ0EsSUFBQSxZQUFBLFdBQUE7UUFDQSxXQUFBOzs7SUFHQSxPQUFBLHFCQUFBLGFBQUEsTUFBQSxXQUFBLE1BQUEsV0FBQSxhQUFBLE9BQUE7OztBQUdBLFNBQUEsY0FBQSxZQUFBLFVBQUE7SUFDQSxJQUFBLFlBQUEsV0FBQTtRQUNBLFdBQUE7OztJQUdBLE9BQUEsb0JBQUEsYUFBQSxNQUFBLFdBQUEsYUFBQSxPQUFBOztBQzFCQTs7QUFFQSxDQUFBLFlBQUE7SUFDQTs7O0lBRUEsUUFBQSxPQUFBLG1CQUFBLFdBQUEsWUFBQTs7O0lBR0EsU0FBQSxTQUFBLFFBQUEsU0FBQSxnQkFBQTtRQUNBLElBQUEsS0FBQTtRQUNBOztRQUVBLFNBQUEsV0FBQTs7S0FFQSIsImZpbGUiOiJhcHAuanMiLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCc7XG5cbihmdW5jdGlvbiAoKSB7XG4gICAgXCJ1c2Ugc3RyaWN0XCI7XG5cbiAgICBhbmd1bGFyLm1vZHVsZSgnYXBwJywgWydhcHAuY29uc3RhbnRzJywgJ2FwcC5ydW4nLCAnYXBwLmNvbmZpZycsICdhcHAucm91dGVzJywgJ2FwcC5jb250cm9sbGVycycsICdhcHAuZGlyZWN0aXZlcycsICdhcHAuZmlsdGVycycsICdhcHAuZmFjdG9yaWVzJywgJ2FwcC5zZXJ2aWNlcycsICdhcHAuYXBpJywgJ3VpLmJvb3RzdHJhcCcsICduZ1Nhbml0aXplJ10pO1xuXG4gICAgYW5ndWxhci5tb2R1bGUoJ2FwcC5jb25zdGFudHMnLCBbXSk7XG4gICAgYW5ndWxhci5tb2R1bGUoJ2FwcC5ydW4nLCBbXSk7XG4gICAgYW5ndWxhci5tb2R1bGUoJ2FwcC5jb25maWcnLCBbXSk7XG4gICAgYW5ndWxhci5tb2R1bGUoJ2FwcC5yb3V0ZXMnLCBbJ3VpLnJvdXRlciddKTtcbiAgICBhbmd1bGFyLm1vZHVsZSgnYXBwLmNvbnRyb2xsZXJzJywgW10pO1xuICAgIGFuZ3VsYXIubW9kdWxlKCdhcHAuZGlyZWN0aXZlcycsIFtdKTtcbiAgICBhbmd1bGFyLm1vZHVsZSgnYXBwLmZpbHRlcnMnLCBbXSk7XG4gICAgYW5ndWxhci5tb2R1bGUoJ2FwcC5mYWN0b3JpZXMnLCBbXSk7XG4gICAgYW5ndWxhci5tb2R1bGUoJ2FwcC5zZXJ2aWNlcycsIFtdKTtcbiAgICBhbmd1bGFyLm1vZHVsZSgnYXBwLmFwaScsIFtdKTtcbn0pKCk7IiwiLy8gKGZ1bmN0aW9uICgpIHtcbi8vICAgICBcInVzZSBzdHJpY3RcIjtcblxuXG4vLyAgICAgYW5ndWxhci5tb2R1bGUoJ2FwcC5jb25maWcnKVxuXG5cbi8vICAgICAgICAgLmNvbmZpZygvKkBuZ0luamVjdCovZnVuY3Rpb24gKCRxUHJvdmlkZXIpIHtcbi8vICAgICAgICAgICAgICRxUHJvdmlkZXIuZXJyb3JPblVuaGFuZGxlZFJlamVjdGlvbnModHJ1ZSk7XG4vLyAgICAgICAgIH0pXG5cblxuLy8gICAgICAgICAuY29uZmlnKC8qQG5nSW5qZWN0Ki9cbi8vICAgICAgICAgZnVuY3Rpb24gKCRodHRwUHJvdmlkZXIpIHtcbi8vICAgICAgICAgICAgICRodHRwUHJvdmlkZXIuaW50ZXJjZXB0b3JzLnB1c2goJ0ludGVyY2VwdG9yJyk7XG4vLyAgICAgICAgIH0pO1xuXG5cbi8vIH0pKCk7XG5cInVzZSBzdHJpY3RcIjsiLCIndXNlIHN0cmljdCc7XG5cbihmdW5jdGlvbiAoKSB7XG4gICAgJ3VzZSBzdHJpY3QnO1xuXG4gICAgYW5ndWxhci5tb2R1bGUoJ2FwcC5jb25zdGFudHMnKS5jb25zdGFudChcIlNJVEVfQ09OU1RBTlRTXCIsIHdpbmRvdy5TSVRFX0NPTlNUQU5UUyk7XG59KSgpOyIsIid1c2Ugc3RyaWN0JztcblxuKGZ1bmN0aW9uICgpIHtcbiAgICBhbmd1bGFyLm1vZHVsZSgnYXBwLmNvbnRyb2xsZXJzJykuY29udHJvbGxlcignQXBwQ3RybCcsIEFwcEN0cmwpO1xuXG4gICAgLyogQG5nSW5qZWN0ICovXG4gICAgZnVuY3Rpb24gQXBwQ3RybCgkcm9vdFNjb3BlLCAkd2luZG93KSB7XG5cbiAgICAgICAgaWYgKCR3aW5kb3cuaW5uZXJXaWR0aCA8IDk5MSkge1xuICAgICAgICAgICAgJHJvb3RTY29wZS5obGtleV91c2VfcGFjZWhvbGRlciA9IHRydWU7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAkcm9vdFNjb3BlLmhsa2V5X3VzZV9wYWNlaG9sZGVyID0gZmFsc2U7XG4gICAgICAgIH1cblxuICAgICAgICAvLyAkcm9vdFNjb3BlLmdldFZpZXcgPSB3aW5kb3cuZ2V0VmlldztcbiAgICAgICAgLy8gJHJvb3RTY29wZS5nZXRDb21tb25WaWV3ID0gd2luZG93LmdldENvbW1vblZpZXc7XG4gICAgfVxufSkoKTsiLCIndXNlIHN0cmljdCc7XG5cbihmdW5jdGlvbiAoKSB7XG4gICAgXCJ1c2Ugc3RyaWN0XCI7XG5cbiAgICBhbmd1bGFyLm1vZHVsZSgnYXBwLnJvdXRlcycpLmNvbmZpZyhmdW5jdGlvbiAoJHN0YXRlUHJvdmlkZXIsICR1cmxSb3V0ZXJQcm92aWRlciwgJGxvY2F0aW9uUHJvdmlkZXIpIHtcbiAgICAgICAgJGxvY2F0aW9uUHJvdmlkZXIuaHRtbDVNb2RlKHRydWUpO1xuXG4gICAgICAgICR1cmxSb3V0ZXJQcm92aWRlci5vdGhlcndpc2UoJy9lcnJvcicpO1xuXG4gICAgICAgICRzdGF0ZVByb3ZpZGVyLnN0YXRlKCdob21lJywge1xuICAgICAgICAgICAgdXJsOiAnLycsXG4gICAgICAgICAgICB0ZW1wbGF0ZVVybDogZ2V0VmlldygnaG9tZScpLFxuICAgICAgICAgICAgY29udHJvbGxlcjogJ0hvbWVDdHJsJyxcbiAgICAgICAgICAgIGNvbnRyb2xsZXJBczogJ0hDJ1xuICAgICAgICB9KS5zdGF0ZSgnZXJyb3InLCB7XG4gICAgICAgICAgICB1cmw6ICcvZXJyb3InLFxuICAgICAgICAgICAgdGVtcGxhdGVVcmw6IGdldFZpZXcoJ2hvbWUnKSxcbiAgICAgICAgICAgIGNvbnRyb2xsZXI6ICdIb21lQ3RybCcsXG4gICAgICAgICAgICBjb250cm9sbGVyQXM6ICdIQydcbiAgICAgICAgfSk7XG4gICAgfSk7XG59KSgpOyIsIid1c2Ugc3RyaWN0JztcblxuZnVuY3Rpb24gaXNVbmRlZmluZWQoZGF0YSkge1xuICAgIHJldHVybiB0eXBlb2YgZGF0YSA9PT0gJ3VuZGVmaW5lZCc7XG59XG5cbmZ1bmN0aW9uIGdldFZpZXcobW9kdWxlTmFtZSwgdmlld05hbWUpIHtcbiAgICBpZiAoaXNVbmRlZmluZWQodmlld05hbWUpKSB7XG4gICAgICAgIHZpZXdOYW1lID0gbW9kdWxlTmFtZTtcbiAgICB9XG4gICAgcmV0dXJuICcuL3ZpZXdzL21vZHVsZXMvJyArIG1vZHVsZU5hbWUgKyAnLycgKyB2aWV3TmFtZSArICcuaHRtbD92PScgKyB3aW5kb3cuaHRtbF92ZXJzaW9uO1xufVxuXG5mdW5jdGlvbiBnZXROZXN0ZWRWaWV3KG1vZHVsZU5hbWUsIHZpZXdOYW1lKSB7XG4gICAgaWYgKGlzVW5kZWZpbmVkKHZpZXdOYW1lKSkge1xuICAgICAgICB2aWV3TmFtZSA9IG1vZHVsZU5hbWU7XG4gICAgfVxuXG4gICAgcmV0dXJuICcuL3ZpZXdzL21vZHVsZXMvJyArIG1vZHVsZU5hbWUgKyAnLycgKyB2aWV3TmFtZSArICcvJyArIHZpZXdOYW1lICsgJy5odG1sP3Y9JyArIHdpbmRvdy5odG1sX3ZlcnNpb247XG59XG5cbmZ1bmN0aW9uIGdldENvbW1vblZpZXcobW9kdWxlTmFtZSwgdmlld05hbWUpIHtcbiAgICBpZiAoaXNVbmRlZmluZWQodmlld05hbWUpKSB7XG4gICAgICAgIHZpZXdOYW1lID0gbW9kdWxlTmFtZTtcbiAgICB9XG5cbiAgICByZXR1cm4gJy4vdmlld3MvY29tbW9uLycgKyBtb2R1bGVOYW1lICsgJy8nICsgdmlld05hbWUgKyAnLmh0bWw/dj0nICsgd2luZG93Lmh0bWxfdmVyc2lvbjtcbn0iLCIndXNlIHN0cmljdCc7XG5cbihmdW5jdGlvbiAoKSB7XG4gICAgJ3VzZSBzdHJpY3QnO1xuXG4gICAgYW5ndWxhci5tb2R1bGUoJ2FwcC5jb250cm9sbGVycycpLmNvbnRyb2xsZXIoJ0hvbWVDdHJsJywgSG9tZUN0cmwpO1xuXG4gICAgLyogQG5nSW5qZWN0ICovXG4gICAgZnVuY3Rpb24gSG9tZUN0cmwoJHN0YXRlLCAkd2luZG93LCBTSVRFX0NPTlNUQU5UUykge1xuICAgICAgICB2YXIgdm0gPSB0aGlzO1xuICAgICAgICBhY3RpdmF0ZSgpO1xuXG4gICAgICAgIGZ1bmN0aW9uIGFjdGl2YXRlKCkge31cbiAgICB9XG59KSgpOyJdfQ==
