var mongoose = require('mongoose'); 

var UserSchema = new mongoose.Schema({
    first_name:{type:String,required:true,max:100},
    last_name:{type:String,required:true,max:100},
    date_of_birth: {type: Date},
    date_of_death: {type: Date}
});

module.exports = mongoose.model('User',UserSchema); 