
function isUndefined(data) {
    return typeof data === 'undefined';
}

function getView(moduleName, viewName) {
    if (isUndefined(viewName)) {
        viewName = moduleName;
    }
    return './views/modules/' + moduleName + '/' + viewName + '.html?v=' + window.html_version;
}

function getNestedView(moduleName, viewName) {
    if (isUndefined(viewName)) {
        viewName = moduleName;
    }

    return './views/modules/' + moduleName + '/' + viewName + '/' + viewName + '.html?v=' + window.html_version;
}

function getCommonView(moduleName, viewName) {
    if (isUndefined(viewName)) {
        viewName = moduleName;
    }

    return './views/common/' + moduleName + '/' + viewName + '.html?v=' + window.html_version;
}


