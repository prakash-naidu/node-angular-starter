(function () {
    'use strict';


    angular
        .module('app.constants')
        .constant("SITE_CONSTANTS", window.SITE_CONSTANTS);
})();