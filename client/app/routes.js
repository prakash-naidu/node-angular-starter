(function () {
    "use strict";

    angular
        .module('app.routes')
        .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
            $locationProvider.html5Mode(true);

            $urlRouterProvider.otherwise('/error');

            $stateProvider

                .state('home', {
                    url: '/',
                    templateUrl: getView('home'),
                    controller: 'HomeCtrl',
                    controllerAs: 'HC'
                })
                
                .state('error', {
                    url: '/error',
                    templateUrl: getView('home'),
                    controller: 'HomeCtrl',
                    controllerAs: 'HC'
                })

        });
})();