(function () {
    "use strict";

    angular.module('app',
        [
            'app.constants',
            'app.run',
            'app.config',
            'app.routes',
            'app.controllers',
            'app.directives',
            'app.filters',
            'app.factories',
            'app.services',
            'app.api',
            
            'ui.bootstrap',
            'ngSanitize',

        ]);

    angular.module('app.constants', []);
    angular.module('app.run', []);
    angular.module('app.config', []);
    angular.module('app.routes', ['ui.router']);
    angular.module('app.controllers', []);
    angular.module('app.directives', []);
    angular.module('app.filters', []);
    angular.module('app.factories', []);
    angular.module('app.services', []);
    angular.module('app.api', []);


})();